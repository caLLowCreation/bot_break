require('dotenv').config();

const express = require('express');
const fetch = require('node-fetch');

const app = express();

const status = {
    stream: { started: false }
};

let breakTimeout = null;

app.get('/bot-started', (req, res) => {
    console.log({statusText: `Bot started...`, time: new Date()});

    if(status.stream.started === true) {
        res.status(200).json(status);
        console.log({statusText: `...Bot started already preformed`, time: new Date()});
        return;
    }

    status.stream.started = true;

    startBreakTimeout();
    
    res.status(200).json(status);

    console.log({statusText: `...Bot started`, time: new Date()});
});

app.get('/break-manual', async (req, res) => {
    console.log({statusText: `Break manual via command...`, time: new Date()});

    clearTimeout(breakTimeout);
    breakTimeout = null;
    
    await sendBreakOn();
});

app.get('/break-closed', (req, res) => {
    console.log({statusText: `Break closed...`, time: new Date()});

    if(status.stream.started === false) {
        res.status(200).json(status);
        console.log({statusText: `...Break closed already preformed`, time: new Date()});
        return;
    }

    clearTimeout(breakTimeout);
    breakTimeout = null;
    
    startBreakTimeout();

    res.status(200).json(status);

    console.log({statusText: `...Break closed`, time: new Date()});
});

app.get('/stream-started', (req, res) => {

    console.log({statusText: `Stream started...`, time: new Date()});

    if(status.stream.started === true) {
        res.status(200).json(status);
        console.log({statusText: `...Stream started already preformed`, time: new Date()});
        return;
    }

    status.stream.started = true;

    clearTimeout(breakTimeout);
    breakTimeout = null;
    
    startBreakTimeout();

    res.status(200).json(status);

    console.log({statusText: `...Stream started`, time: new Date()});
});

app.get('/stream-ended', (req, res) => {

    console.log({statusText: `Stream ended...`, time: new Date()});

    if(status.stream.started === false) {
        res.status(200).json(status);
        console.log({statusText: `...Stream ended already preformed`, time: new Date()});
        return;
    }

    status.stream.started = false;

    clearTimeout(breakTimeout);
    breakTimeout = null;
    
    res.status(200).json(status);
    console.log({statusText: `...Stream ended`, time: new Date()});
});

app.get('*', (req, res) => {
    res.status(403).send('(Forbidden)');
});

if (module === require.main) {

    const server = app.listen(process.env.PORT || 9000, async () => {
        const port = server.address().port;

        console.log({statusText: port, time: new Date()});
        try {

        } catch (error) {
            console.log({statusText: error, time: new Date()});
        }

    });
}

function startBreakTimeout() {
    if(breakTimeout) {
        console.log({ error: null, status: 204, statusText: `The value of breakTimeout is not null`, time: new Date() });
    }

    console.log({ error: null, status: 204, statusText: `Starting break tracker timeout`, time: new Date() });
    
    breakTimeout = setTimeout(async () => {
        if(status.stream.started === false) {
            console.log({ error: null, status: 204, statusText: `The stream has ended` });
            return;
        }
            
        await sendBreakOn();
    }, 1000 * 60 * 60);
}

module.exports = app;

async function sendBreakOn() {
    try {
        const result = await fetch(`http://localhost:1338/break`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ state: 'on' })
        });
        console.log({ error: null, status: 200, statusText: `Response received`, result, time: new Date() });
    }
    catch (error) {
        console.log({ error, status: 500, statusText: 'Internal Server Error (ping error)', time: new Date() });
    }
}
